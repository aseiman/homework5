//import static org.junit.Assert.assertEquals;

import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode (String n, TreeNode d, TreeNode r) {
	   this.name = n;
	   this.firstChild = d;
	   nextSibling = r;	   	   	 
   }
  
   private void printMe() {
	   if (name != null) System.out.println("name:" + name);
	   if (firstChild != null) System.out.println("firstChild:" + firstChild.name);
	   if (nextSibling != null) System.out.println("nextSibling:" + nextSibling.name);
   }

   
   public static TreeNode parsePrefix (String s) {
	   
	   // String to short, then return empty TreeNode
	   if (s == null || s.length() == 0) new RuntimeException("Empty string as input.");
	   
	   // Test TreeNode string
	   testString(s);
	   
	   // Parse name
	   String nodeName = parseName (s);
	   if (nodeName.contains("\t") || nodeName.contains(" ") || nodeName.contains("(") || nodeName.contains(")") || nodeName.contains(",")) {
		   throw new RuntimeException("Illegal characters in TreeNode name: " + nodeName);
	   } else if (nodeName.length() == 0) {
		   throw new RuntimeException("TreeNode has to have a name.");
	   }
	   s = s.substring(nodeName.length(), s.length());
	   
	   // Parse children string
	   List<String> children = new ArrayList<String>();
	   if (s.length() > 0 && s.charAt(0) == '(') {
		   children = parseChildren (s);
	   } else {
		   return new TreeNode(nodeName, null, null);
	   }	   
	   
	   // Create children nodes
	   children.add("");
	   TreeNode child = null;
	   for (int i = children.size() - 2; i >= 0; i--) {
		   TreeNode tmp = TreeNode.parsePrefix(children.get(i));
		   tmp.nextSibling = child;
		   child = tmp;		   
	   }
	   
	   return new TreeNode(nodeName, child, null);
   }
   
        
   
   private static void testString(String s) {
	   
	   // Test illegal combination of characters in string
	   if (s.contains(",,") || s.contains("(,") || s.contains(",)")) throw new RuntimeException("Illegal string for parsing: " + s);		   
	   if (s.contains("()")) throw new RuntimeException("Empty subtree in string for parsing: " + s);
	   
	   // Test the number of brackets
	   int i = s.length() - s.replace("(", "").length();
	   int j = s.length() - s.replace(")", "").length();
	   if (i != j) throw new RuntimeException("Numbers of brackets do not match: " + s);
		   
	   // Test that string starts and end with proper brackets
	   for (i=0;i < s.length(); i++) {
		   if (s.charAt(i) == '(') {
			   break;
		   } else if (s.charAt(i) == ')') {
			   throw new RuntimeException("Wrong bracket combination: " + s);
		   }
	   }
	   
	   // Test that string has head
	   for (i=0;i < s.length(); i++) {
		   if (s.charAt(i) == '(') {
			   break;
		   } else if (s.charAt(i) == ',') {
			   throw new RuntimeException("Tree does not have head: " + s);			   
		   }
	   }
		   
}


private static String parseName (String s) {	   
	   int i = 0;
	   for (i = 0; i < s.length(); i++) {
		   if (s.charAt(i) == '(' || s.charAt(i) == ')' || s.charAt(i) == ',') {
			   break;
		   }
	   }
	   return s.substring(0, i);
   }
   
   private static List<String> parseChildren (String s) {
	   List<String> children = new ArrayList<String>();
	   int i = 0;
	   int j = 0;
	   int k = 0;
	   for (i = 0; i<s.length(); i++) {
		   if (s.charAt(i) == '(') {
			   j++;
		   } else if (s.charAt(i) == ')') {
			   if (j==0) {
				   break;				   
			   } else if (j==1) {
				   children.add(s.substring(k+1, i));
				   k = i;				   
			   } else {
				   j--;
			   }	   
		   } else if (s.charAt(i) == ',' && j==1) {			   
			   children.add(s.substring(k+1, i));
			   k = i;
		   }
	   }
	   return children;
   }
     
   
   private boolean hasNextSibling() {
	   return !(this.nextSibling == null);
   }
   
   
   private boolean hasChild() {
	   return !(this.firstChild== null);
   }
   
   
   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();

      b.append(name);
  
      if (this.hasNextSibling()) {
    	  b.append(",");
    	  b.append(this.nextSibling.rightParentheticRepresentation());
      }
      
      if (this.hasChild()) {
    	  b.insert(0, ")");
    	  b.insert(0, this.firstChild.rightParentheticRepresentation());
    	  b.insert(0, "(");
      }                      
      
      return b.toString();
   }
   

   public static void main (String[] param) {
      String s = "A(B1,C,D)";
      TreeNode t = TreeNode.parsePrefix (s);
      System.out.println(s);

      TreeNode.parsePrefix("A1(B1,C(E,F,G),D(T1,T2,T3,T4))");
      String r = t.rightParentheticRepresentation();
	  System.out.println(r);
      
      s = "A(B1,C(E,F,G),D(T1,T2,T3,T4))";
      t = TreeNode.parsePrefix (s);
      r = t.rightParentheticRepresentation();
      System.out.println(s);
      System.out.println(r);
      
   }
}
